FROM node:alpine as builder
ENV packages python make g++
WORKDIR /usr/src/app
COPY webapp/ ./
RUN apk add --no-cache $packages && \
    npm install && \
    npm run-script build

FROM nginx
COPY --from=builder /usr/src/app/dist/ /usr/share/nginx/html/
EXPOSE 80